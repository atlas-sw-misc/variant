#include <variant.hpp>
const char json[] = " {   \"hello\" : \"world\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, \"pi\": 3.1416, \"a\":[1.1, 2.1, 3.1, 4.1] } ";
int main(int argc,char *argv[]){
  variant32 v,w;
  variant j;
  std::cout << j.type_name() << std::endl;
  uint32_t n_Row=3; //,n_Col=3;
  // Layout is one array per column
  for (unsigned row=1; row<=n_Row; row++) {
      j[row-1] = row;
    }

  std::cout << j.type_name() << std::endl;
  j.dump();
  v["test"]= {
    {"a",20} , {"b",40}, {"string","mystring"}    
  };
  v["some_double"]=double(1.5);
  std::cout << "some double type" << v["some_double"].type_name() << std::endl;
  v.dump();
  const std::string &s= v["test"]["string"];
  std::cout << "s=" << s << std::endl;
  v.dump();
  w=v;
  w["test"]["a"]=25;
  w.dump();
  v.dump();
  for(auto l:w) l.dump();
  std::vector<unsigned> vec={1,2,3,4,5};
  v["array"]=vec;
  std::vector<unsigned>  v2=v["array"];
  //std::vector<unsigned> const &aaa=v["array"];
  //aaa={4,5,6};


  v.dump();
  variant x;
  x.fromJson(json);
  x.dump();
  std::cout << "X[a].typename(): "<< x["a"].type_name() << std::endl;
  x.fromJson(json,true,true);
  std::cout << "X[a].typename(): "<< x["a"].type_name() << std::endl;
  x.dump();
  variant32 sa;
  sa["array"]=variant32::array(); // empty array assigned to key "array"
  sa.dump();
  sa["array"].push_back("string3");
  sa["array"].push_back("string4");
  sa.dump();
  variant32 sb;
  sb["test1"]="test2";
  sb["a"]["b"]="c";
  std::string ssss=sb["a"]["b"];
  std::string dddd;
  dddd=sb["a"]["b"];
  std::cout << dddd<< std::endl;
  sb["t"]=5;
  sb.dump();
  std::cout << sb << std::endl;
  std::cout << (sb["test1"]=="test2") <<std::endl;
  std::cout << (sb["t"]==5) <<std::endl;
  std::cout << sb.byte_count() << std::endl;
  variant eeee;
  eeee["test"]="test";
  auto it=eeee.find("test");
it->dump() ;
  variant &ref=eeee;
  std::string es=ref["test"];
  variant::StrPtr sr("test");
  std::string &rf=sr;
  rf+=" add something";
  std::cout << rf <<std::endl;
  variant nullp=nullptr;
  nullp.dump();
std::cout << sa.contains("array") <<std::endl ; 
std::cout << sa.contains("random") <<std::endl ;

 variant ctest;
 ctest["1"]["2"]["3"]="test";
 std::cout << "ctest\n";
 std::cout << ctest.contains({"1","2","3"}) << std::endl;
 std::cout << ctest.contains({"5","4"}) << std::endl;
 ctest["1"]["2"].erase("3");
 ctest.dump();

}
